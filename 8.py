def reverse_my_list(lst):
    if (len(lst) > 0):
        lst[0], lst[-1] = lst[-1], lst[0]
    return lst
    
print(reverse_my_list([1, 2, 3, 4]))
