def sum_range(a, z):
    if a > z:
        a, z = z, a
    s = 1
    for i in range (a, z+1):
        s *= i
    return s
    
d,b = map(int, input().split())
print(sum_range(d, b))