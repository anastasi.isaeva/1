import time
def decorator(func):
    def inner(*args, **kwargs):
        a = time.time()
        b = time.time() - a
        res = func(*args, **kwargs)
        print(b)
        return res

    return inner

@decorator
def f(x, y):
    return x + y


f(10, 2)

